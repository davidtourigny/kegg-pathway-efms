""" Tools for generating rxn to gene mappings for KGML models using KEGG API.
    """

from xml.etree import ElementTree
import requests, re, csv
import os

def rxn_to_gene(sbml_directory, pathway, database = 'Ensembl'):
    
    # get rxn to gene mappings for pathway KGML model
    # possible settings for database are (human): NCBI-GeneID, NCBI-ProteinID, OMIM, HGNC, Ensembl, Vega, Pharos, UniProt
    # If a gene does not contain an entry for selected database then it will output KEGG id by default!
    # writes rxn to gene mapping to csv file in current working directory

    kgml_dir_name = os.path.join(sbml_directory, 'kgml_files')
    kgml_file_name = os.path.join(kgml_dir_name, pathway + '.xml')

    if os.path.exists(kgml_file_name):
        database += ':'
        tree = ElementTree.parse(kgml_file_name)
        root = tree.getroot()
        entries = root.findall('entry')
    
        output_dir_name = os.path.join(os.path.dirname(__file__), 'rxn_to_gene')
        if not os.path.exists(output_dir_name):
            os.makedirs(output_dir_name)
    
        with open(os.path.join(output_dir_name, pathway + '.csv'), 'w') as file:
            writer = csv.writer(file)
    
            for entry in entries:
                if entry.attrib['type'] == 'gene' and 'reaction' in entry.attrib:
                    reactions = entry.attrib['reaction'].split()
                    genes = entry.attrib['name'].split()
                
                    gene_ids = []
                    for gene in genes:
                        gene_db_string = requests.get('http://rest.kegg.jp/get/' + gene).content.decode('utf-8')
                        dblinks = gene_db_string.split('DBLINKS')[1].split('AASEQ')[0].split()
                        if database in dblinks:
                            gene_id = dblinks[dblinks.index(database)+1]
                        else:
                            gene_id = gene
                        gene_ids.append(gene_id)

                    for rxn in reactions:
                        row = [rxn[3:len(rxn)]]
                        row.extend(gene_ids)
                        writer.writerow(row)
    else:
        print("Can not find KGML file " + kgml_file_name)

def process_all(sbml_directory, database = 'Ensembl'):
    
    # generate reaction to gene mappings for all files in sbml_directory
    
    pathway_file_name = os.path.join(sbml_directory, 'pathways.txt')
    if os.path.exists(pathway_file_name):
        print("Generating reaction to gene mappings for pathways in " + pathway_file_name)
        pathway_file = open(pathway_file_name, 'r')
        pathways = pathway_file.read().splitlines()
        for pathway in pathways:
            rxn_to_gene(sbml_directory,pathway,database)
    else:
        print("Can not find pathway file " + pathway_file_name)



