""" Tools for generating EFMs for SBML models using pyefm.
    """

from pyefm import calculate_elementary_modes
from xml.etree import ElementTree
import cobra
import libsbml
import os, re

def get_model(sbml_directory, pathway, reversibility = True):
    
    # Generate modified cobra model using missing information from KGML and SBML
    # Add exchange reactions: solution to issue https://github.com/draeger-lab/KEGGtranslator/issues/8
    # is use KGML maplink relations to get notes from subtype and identify with species id from SBML
    # Retain reversibility (optional): temporary solution to issue https://github.com/opencobra/cobrapy/issues/976
    
    sbml_dir_name = os.path.join(sbml_directory, 'sbml_files')
    kgml_dir_name = os.path.join(sbml_directory, 'kgml_files')
    sbml_file_name = os.path.join(sbml_dir_name, pathway + '.sbml.xml')
    kgml_file_name = os.path.join(kgml_dir_name, pathway + '.xml')

    if os.path.exists(sbml_file_name) and os.path.exists(kgml_file_name):
        model = cobra.io.read_sbml_model(sbml_file_name)
        sbml = libsbml.readSBMLFromFile(sbml_file_name)
        kgml_tree = ElementTree.parse(kgml_file_name)
        kgml_root = kgml_tree.getroot()
    
        relations = kgml_root.findall('relation')
        entries = kgml_root.findall('entry')
        species = sbml.model.species
        boundary_species = []
        for rel in relations:
            if rel.attrib['type'] == 'maplink':
                for subtype in rel:
                    value = subtype.attrib['value']
                    for entry in entries:
                        if entry.attrib['id'] == value:
                            name = entry.attrib['name'].replace('cpd:','')
                            for compound in species:
                                notes = compound.notes.toXMLString()
                                if re.search(name,notes):
                                    if compound.id not in boundary_species:
                                        boundary_species.append(compound.id)

        for met in boundary_species:
            ex_rxn = cobra.Reaction('EX_' + met)
            ex_rxn.name = met + ' exchange reaction'
            ex_rxn.upper_bound = 1000.0
            ex_rxn.lower_bound = -1000.0
            model.add_reaction(ex_rxn)
            ex_rxn.add_metabolites({met: -1.0})
                
        if reversibility:
            for rxn in sbml.model.reactions:
                if not rxn.reversible:
                    model.reactions.get_by_id(rxn.id).lower_bound = 0.0

        return model
    
    elif os.path.exists(sbml_file_name):
        print("Can not find KGML file " + kgml_file_name)
    elif os.path.exists(kgml_file_name):
        print("Can not find SBML file " + sbml_file_name)

def calc_efms(sbml_directory, pathway, remove_loops = True, reversibility = True):
    
    # calculate EFMs for given pathway for model created by get_model
    # discard loopy EFMs by removing those without exhange reaction in support (optional)
    # writes EFMs to CSV format in current working directory

    model = get_model(sbml_directory, pathway, reversibility)
    if model:
        efms = calculate_elementary_modes(model, opts={'normalize': 'min'}, verbose=True, java_args='-Xmx1g')
    
        if remove_loops:
            exchange_ids = []
            for rxn in model.reactions:
                if not re.search("rnR\d{1,5}",rxn.id):
                    exchange_ids.append(rxn.id)
            efms = efms[efms[exchange_ids].any(axis='columns')]

        output_dir_name = os.path.join(os.path.dirname(__file__), 'efm_files')
        if not os.path.exists(output_dir_name):
            os.makedirs(output_dir_name)
        efm_file_name = os.path.join(output_dir_name, pathway + '.csv')
        efms.to_csv(efm_file_name)

    else:
        print("Problem generating model for pathway " + pathway)

def process_all(sbml_directory):
    
    # generate EFM files for all files in sbml_directory
    
    pathway_file_name = os.path.join(sbml_directory, 'pathways.txt')
    if os.path.exists(pathway_file_name):
        print("Generating EFM files for pathways in " + pathway_file_name)
        pathway_file = open(pathway_file_name, 'r')
        pathways = pathway_file.read().splitlines()
        for pathway in pathways:
            calc_efms(sbml_directory,pathway)
    else:
        print("Can not find pathway file " + pathway_file_name)



