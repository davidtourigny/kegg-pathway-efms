""" Tools for generating gene sets in GMT format from EFMs of KEGG pathways.
    Reaction to gene mappings and EFM files are assumed to exist in output directories
    created by rxn_to_gene.py and calc_efms.py, respectively, in working directory.
    """

import pandas as pd
import csv
import os

def calc_gene_set(elementary_flux_mode, pathway, rxn_to_gene):

    # generate gene set from efm for given pathway (pandas Series object) based on reaction to gene mapping
    # returns a list in GMT format: [gene set name, comment (can be None), gene1, gene2, ...]
    # this function can be customised but return type must conform with above GMT format or be None
    
    gene_set = [pathway + ':' + elementary_flux_mode.name, None]
    for rxn, coeff in elementary_flux_mode.items():
        rxn = rxn.replace('rn','')
        if coeff != 0.0 and rxn in rxn_to_gene:
            gene_set.extend(rxn_to_gene[rxn])
    if len(gene_set) > 2:
        return gene_set
    else:
        return None

def calc_gene_sets(pathway, feature_dict = None):

    # generate gene sets for EFMs of given KEGG pathway
    # if feature_dict provided, used to first convert gene ids
    # writes gene sets to GMT format in current working directory

    efm_file_name = os.path.join(os.path.dirname(__file__), 'efm_files/' + pathway + '.csv')
    r2g_file_name = os.path.join(os.path.dirname(__file__), 'rxn_to_gene/' + pathway + '.csv')

    if os.path.exists(efm_file_name) and os.path.exists(r2g_file_name):
        efm_df = pd.read_csv(efm_file_name,index_col=0)
        rxn_to_gene = {}
        with open(r2g_file_name) as r2g_file:
            reader = csv.reader(r2g_file)
            for row in reader:
                rxn = row.pop(0)
                genes = row
                if feature_dict:
                    for gene in genes:
                        if gene in feature_dict:
                            genes[genes.index(gene)] = feature_dict[gene]
                        else:
                            print("Retaining gene id " + gene + " because not in feature file")
                rxn_to_gene[rxn] = genes
    
        gene_sets = efm_df.apply(calc_gene_set,args = (pathway,rxn_to_gene), axis=1)

        output_dir_name = os.path.join(os.path.dirname(__file__), 'gene_sets')
        if not os.path.exists(output_dir_name):
            os.makedirs(output_dir_name)

        with open(os.path.join(output_dir_name, pathway + '.gmt'), 'wt') as file:
            writer = csv.writer(file, delimiter='\t')
            for efm, gene_set in gene_sets.items():
                if gene_set is not None:
                    writer.writerow(gene_set)
    
    elif not os.path.exists(efm_file_name):
        print("Ignoring pathway " + pathway + ": file " + efm_file_name + " does not exist")

    elif not os.path.exists(r2g_file_name):
        print("Ignoring pathway " + pathway + ": file " + r2g_file_name + " does not exist")

def process_all(sbml_directory, feature_file_name = None):
    
    # generate GMT files for all pathways in sbml_directory
    # optionally supply path to feature file to use for gene id conversions
    # last step merges all gene sets into single output file
    
    feature_dict = None
    if feature_file_name:
        if os.path.exists(feature_file_name):
            feature_dict = {}
            with open(feature_file_name) as feature_file:
                reader = csv.reader(feature_file, delimiter='\t')
                for row in reader:
                    key = row.pop(0)
                    feature_dict[key] = row[0]
        else:
            print("Can not find feature file " + feature_file_name + ". Proceeding without gene id conversions.")
    
    pathway_file_name = os.path.join(sbml_directory, 'pathways.txt')
    if os.path.exists(pathway_file_name):
        print("Generating gene sets for pathways in " + pathway_file_name)
        pathway_file = open(pathway_file_name, 'r')
        pathways = pathway_file.read().splitlines()
        for pathway in pathways:
            calc_gene_sets(pathway,feature_dict)
        
        print("Merging gene sets into file all_gene_sets.gmt")
        gmt_dir_name = os.path.join(os.path.dirname(__file__), 'gene_sets')
        with open(os.path.join(gmt_dir_name, 'all_gene_sets.gmt'), 'wt') as file:
            writer = csv.writer(file, delimiter='\t')
            for gmt in os.listdir(gmt_dir_name):
                gmt_file_name = gmt_dir_name + '/' + gmt
                with open(gmt_file_name) as gmt_file:
                    reader = csv.reader(gmt_file, delimiter='\t')
                    for row in reader:
                        if row:
                            writer.writerow(row)

    else:
        print("Can not find pathway file " + pathway_file_name)


