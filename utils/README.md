# Python utilities

This directory includes the Python utilities:

* [calc_efms](./calc_efms.py): for calculating EFMs from SBML files using [pyefm](https://github.com/pstjohn/pyefm), a Python wrapper for [EFMtool](https://csb.ethz.ch/tools/software/efmtool.html)

* [rxn_to_gene](./rxn_to_gene.py): for assembling reaction to gene mappings from KGML pathway files using the [KEGG API](https://www.kegg.jp/kegg/rest/keggapi.html) and

* [gene_sets](./gene_sets.py): for assembling gene sets from EFMs of [KEGG](https://www.genome.jp/kegg/) pathways

## Generating EFMs and reaction to gene mappings for KEGG pathways

Both utilities [rxn_to_gene](./rxn_to_gene.py) and [calc_efms](./calc_efms.py) can be run locally from this directory provided the dependencies (below) are installed. To process all pathways listed in the file [pathways](../sbml/pathways.txt) in the directory [sbml](../sbml/) do the following in an interactive Python session (from this working directory):
```
from rxn_to_gene import process_all
process_all("../sbml/")
```

for generating reaction to gene mappings, and:
```
from calc_efms import process_all
process_all("../sbml/")
```

for generating EFM files. If [pyefm](https://github.com/pstjohn/pyefm) is not installed to your system the latter command can be used after copying [calc_efms](./calc_efms.py) to [pyefm](https://github.com/pstjohn/pyefm) after cloning that repository and running the interactive Python session from there.

Generating gene sets using [gene_sets](./gene_sets.py) for each pathway is similarly achieved (assuming the output directories from above steps are present in working directory) in an interactive Python session using the commands:
```
from gene_sets import process_all
process_all("../sbml/")
```
Here there is also an option to convert between the gene ids used in [rxn_to_gene](./rxn_to_gene.py) (default Ensembl) and feature labels by providing a second argument to the ``process_all`` method:
```
from gene_sets import process_all
process_all("../sbml/", feature_file_name = "XXX.tsv")
```
where ``"XXX.tsv"`` is the path to the feature file.

## Dependencies

General dependencies:

* [Python](https://www.python.org/) version 3.6 or higher

Dependencies for [calc_efms](./calc_efms.py):

* [cobra](https://opencobra.github.io/cobrapy/): best obtained using ``pip install cobra``
* [pyefm](https://github.com/pstjohn/pyefm) written by [Peter St. John](https://github.com/pstjohn)

Dependencies for [rxn_to_gene](./rxn_to_gene.py):

* [requests](https://requests.readthedocs.io): best obtained using ``pip install requests``

Dependencies for [gene_sets](./gene_sets.py):

* [pandas](https://pandas.pydata.org/): best obtained using ``pip install pandas``
