# KGML downloads and SBML conversions

This directory includes the following:

* [build_sbml](./build_sbml.sh): script for obtaining and running [KEGG translator](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/) to generate SBML models from KEGG pathways listed in [pathways.txt](./pathways.txt)

* [pathways.txt](./pathways.txt): list of pathways for generating SBML files

The script [build_sbml](./build_sbml.sh) can be run provided ``KEGGtranslator_v2.5.jar``-- version 2.5 of [KEGGtranslator](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/) available for download [here](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/downloads/index.htm)-- is copied to the working directory. After running [build_sbml](./build_sbml.sh), KGML files for pathways in [pathways.txt](./pathways.txt) and SBML corresponding SBML files will appear in subdirectories ``kgml_files`` and ``sbml_files``, respectively to be used downstream by the Python [utilities](../utils).

## Dependencies

Dependencies:

* [KEGGtranslator](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/) version 2.5 available for download [here](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/downloads/index.htm)
