#!/usr/bin/env bash

mkdir kgml_files
mkdir sbml_files
while read pathway; do
    echo "Downloading KGML for pathway $pathway"
    curl http://rest.kegg.jp/get/${pathway}/kgml -o kgml_files/${pathway}.xml
    echo "Converting KGML for pathway $pathway"
    java -jar KEGGtranslator_v2.5.jar --input ./kgml_files/${pathway}.xml --output ./sbml_files/${pathway}.sbml.xml --format SBML --autocomplete-reactions false
done <pathways.txt
