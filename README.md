# KEGG pathway EFM gene set analysis

This project includes the following directories:

* [utils](./utils/): utilities for reaction to gene mappings using the [KEGG API](https://www.kegg.jp/kegg/rest/keggapi.html), EFM enumeration using [EFMtool](https://csb.ethz.ch/tools/software/efmtool.html), and generating files from these for downstream gene set analysis

* [sbml](./sbml/): list of KEGG pathway maps for [human metabolism](https://www.genome.jp/kegg-bin/show_organism?menu_type=pathway_maps&org=hsa) and script for converting these to SBML format using [KEGG translator](http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/)
